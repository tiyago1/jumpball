﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RatioElement<T>
{
    public T Value { get; private set; }
    public int Ratio { get; private set; }

    public RatioElement(T value, int ratio)
    {
        Value = value;
        Ratio = ratio;
    }

    public override string ToString()
    {
        return "Ratio : " + Ratio.ToString() + "Value : " + Value.ToString();
    }
}

public class RandomRatioCalculater<T>
{
    public List<RatioElement<T>> RatioElements { get; private set; }

    public RandomRatioCalculater(List<RatioElement<T>> elements)
    {
        if (elements != null)
        {
            int sum = elements.Sum(it => it.Ratio);

            if (sum != 100)
            {
                foreach (var item in elements)
                    Debug.LogError("!" + item);

                throw new System.Exception("Sum is not 100. Sum : " + sum);
            }
            else
            {
                RatioElements = elements;
            }
        }
    }

    public RatioElement<T> GetRandomValue()
    {
        int totalRatio = RatioElements.Sum(it => it.Ratio);

        //foreach (var item in RatioElements)
        //{
        //    Debug.Log(item);
        //}
        List<T> ratiotedList = CreateRatiotedList();

        int randomIndex = Random.Range(1, totalRatio - 1);
        var result = RatioElements.First(it => it.Value.Equals(ratiotedList[randomIndex]));
        return result;
    }

    private List<T> CreateRatiotedList()
    {
        List<T> ratiotedValueList = new List<T>();

        for (int i = 0; i < RatioElements.Count; i++)
        {
            for (int z = 0; z < RatioElements[i].Ratio; z++)
            {
                ratiotedValueList.Add(RatioElements[i].Value);
            }
        }
        ratiotedValueList.Distinct();
        JumpLogger.Log<string>("Elements", false);
        foreach (var item in RatioElements)
        {
            JumpLogger.Log<string>(string.Format("{0} tane {1}", ratiotedValueList.Count(it => it.Equals(item.Value)), item));
        }

        return ratiotedValueList;
    }

    public void UpdateRatioElements(List<RatioElement<T>> elements)
    {
        RatioElements = elements;
    }
}