﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    #region Constants

    public const float VERTICAL_MOVEMENT_VALUE = 4.0f;

    #endregion

    #region Fields

    [Header("Managers")]
    public UIManager UIManager;
    public HoopManager HoopManager;
    public CameraManager CameraManager;
    public DifficultyManager DifficultyManager;
    public CurrencyManager CurrencyManager;

    [SerializeField] private GameObject ball;

    private bool isPlayingGame;

    #endregion

    #region Variable

    public GameData GameData { get; private set; }
    public PlayerData PlayerData { get; private set; }

    #endregion

    #region Unity Methods

    public void Start()
    {
        Initialize();
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
           
        }
    }

    #endregion

    #region Private Methods

    private void Initialize()
    {
        isPlayingGame = false;
        SubscribeEvents();
        StartGame();
        UIManager.UpdateStarText(PlayerData.StarCount);
    }

    private void SubscribeEvents()
    {
        BallSystem.Components.Events.OnBallPotted += BallEvents_OnBallPotted;
        BallSystem.Components.Events.OnStarGained += BallEvents_OnStarGained;
        BallSystem.Components.Events.OnPointerDownExecuted += BallEvents_OnPointerDownExecuted;
        BallSystem.Components.Events.OnBallFalledDown += BallEvents_OnBallFalledDown;
    }

    private void StartGame()
    {
        DifficultyManager = new DifficultyManager();
        DifficultyManager.Initialize();
        PlayerData = new PlayerData();
        GameData = new GameData();
        CurrencyManager = new CurrencyManager();
        HoopManager.Initialize(ball);
        HoopManager.FirstHoopGenerate();
    }

    private void CalculateAndUpdateGameData()
    {
        GameData.PottedBallCount++;

        if (HoopManager.IsPerfectScore())
        {
            GameData.PerfectScoreCount++;
            GameData.TotalPerfectScoreCount++;
        }
        else
        {
            GameData.PerfectScoreCount = 0;
        }

        GameData.HoopScore = GameData.PerfectScoreCount + GameData.DEFAULT_HOOP_SCORE;
        GameData.Score += GameData.HoopScore;
    }

    // HoopType değil state yapılabilir.
    private void NextHoop()
    {
        DifficultyManager.UpdateDifficulty(GameData);
        HoopType selectedHoopType = DifficultyManager.GetNextHoopType();

        //JumpLogger.Log(selectedHoopType, true);
        bool isShowStar = CurrencyManager.ShouldShowStar(GameData, PlayerData);
        HoopManager.NextHoopGenerate(selectedHoopType, isShowStar);

        HoopManager.GetActiveHoopsStar().OnStarReachedToTarget += GameManager_OnStarReachedToTarget;
    }

    #endregion

    #region Events

    private void BallEvents_OnBallPotted()
    {
        CalculateAndUpdateGameData();
        HoopManager.HoopPassed();
        UIManager.ShowHoopScores(GameData, HoopManager.GetLastCreatedPosition());
        StartCoroutine(WaitForSecondsAndPlayCameraMotion());
    }

    private void BallEvents_OnBallFalledDown()
    {
            UIManager.OnRestartButtonClicked();
    }

    private void BallEvents_OnPointerDownExecuted()
    {
        if (!isPlayingGame)
        {
            UIManager.HideObjectsForStarter();
            UIManager.ShowObjectsForStarter();
            isPlayingGame = true;
        }
    }

    private void BallEvents_OnStarGained()
    {
        PlayerData.StarCount++;
        GameData.StarCount++;
    }

    private void OnCameraMotionFinished()
    {
        BallSystem.Components.Launcher.ResetLauncher();
        NextHoop();
    }

    private void GameManager_OnStarReachedToTarget()
    {
        HoopManager.GetActiveHoopsStar().OnStarReachedToTarget -= GameManager_OnStarReachedToTarget;

        UIManager.UpdateStarText(PlayerData.StarCount);
    }

    #endregion

    #region Coroutines

    private IEnumerator WaitForSecondsAndPlayCameraMotion()
    {
        yield return new WaitForSeconds(0.5f);
        CameraManager.ShakeCamera();
        CameraManager.MoveCameraVerticalDirection(ball.transform.position, OnCameraMotionFinished);
    }

    #endregion
}