using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Random = UnityEngine.Random;

public enum HoopType
{
    Default,
    Higher,
    Angle,
    VerticalMotion,
    Walled
}

public enum WallDirection
{
    Left,
    Right
}

public class HoopManager : MonoBehaviour
{
    #region Variables

    private List<Hoop> hoops; // only exist one hoop ?

    private float lastXPosition;
    private float[] xRange = new float[]
    {
        0.2f,
        2.0f,
        4.0f,
        5.4f,
    };

    private float lastYPosition;
    private float[] yRange = new float[]
    {
        5.0f,
        6.0f,
        7.0f,
    };

    private Vector2 lastCreatedHoopPosition;

    public float lastRotateAngle;
    private float[] rotateAngles = new float[]
    {
        25,
        0,
        -25,
    };

    private GameObject ball;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        Initialize();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            JumpLogger.Log(GetRandomRotation());
        }
    }

    #endregion

    #region Public Methods

    public void Initialize(GameObject ball)
    {
        this.ball = ball;
    }

    public void NextHoopGenerate(HoopType hoopType, bool isShowStar)
    {
        DisableAllHoops();
        HoopData hoopData = new HoopData();
        float x = GetRandomXPositionWithOutX(lastXPosition);
        float y = yRange[0];
        float angle = rotateAngles[1];
        WallDirection? wallDirection = null;
        switch (hoopType)
        {
            case HoopType.Default:
                y = yRange[0];
                angle = rotateAngles[1];
                break;
            case HoopType.Angle:
                y = GetRandomYPositionWithOutY(lastYPosition);
                angle = GetFlipRotation();
                break;
            case HoopType.Higher:
                y = GetRandomYPositionWithOutY(lastYPosition);
                break;
            case HoopType.VerticalMotion:
                y = GetRandomYPositionWithOutY(lastYPosition);
                angle = GetRandomRotation();
                break;
            case HoopType.Walled:
                y = GetRandomYPositionWithOutY(lastYPosition);
                wallDirection = GetWallDirection();
                break;
            default:
                break;
        }


        hoopData.Position = new Vector2(x, GetLastCreatedPosition().y + y);
        hoopData.Angle = angle;
        hoopData.Type = hoopType;
        hoopData.IsVisible = true;
        hoopData.WallDirection = wallDirection;
        hoopData.HasStar = isShowStar;
        CreateHoop(hoopData);
    }

    public void FirstHoopGenerate()
    {
        lastXPosition = xRange[0];
        lastYPosition = 0.6f;

        HoopData hoopData = new HoopData();
        hoopData.Position = new Vector2(lastXPosition, lastYPosition);
        hoopData.Angle = rotateAngles[1];
        hoopData.Type = HoopType.Default;
        hoopData.IsVisible = true;
        CreateHoop(hoopData);
    }

    public Vector2 GetLastCreatedPosition()
    {
        return lastCreatedHoopPosition;
    }

    public bool IsPerfectScore()
    {
        return hoops.First().IsPerfectScore;
    }

    public void HoopPassed()
    {
        GetCurrentHoop().SetColor(Color.gray, 1.0f);
        GetCurrentHoop().GetStar().HideStar();
        GetCurrentHoop().PlayScroreParticle();
        GetCurrentHoop().SetTriggerActive(false);
        GetCurrentHoop().StopVerticalMotion();
        GetCurrentHoop().PlayHoopedAnimation();
    }

    public Star GetActiveHoopsStar()
    {
        return GetCurrentHoop().GetStar();
    }

    #endregion

    #region Private Methods

    private void Initialize()
    {
        hoops = this.GetComponentsInChildren<Hoop>().ToList();
        DisableAllHoops();
    }

    private void CreateHoop(HoopData hoopData)
    {
        Hoop availableHoop = GetAvailableHoop();
        availableHoop.UpdateData(hoopData);
        lastCreatedHoopPosition = availableHoop.transform.position;
    }

    private void DisableAllHoops()
    {
        hoops.ForEach(it => it.SetVisibilty(false));
    }

    private Hoop GetCurrentHoop()
    {
        return hoops.First(it => it.isActiveAndEnabled);
    }

    private Hoop GetAvailableHoop()
    {
        return hoops.First(it => !it.isActiveAndEnabled);
    }

    private float GetRandomYPositionWithOutY(float previousYPosition)
    {
        List<float> removedLastYPosition = yRange.Where(it => it != previousYPosition).ToList();

        //if (previousYPosition > yRange[3])
        //    removedLastYPosition = removedLastYPosition.Where(it => it < previousYPosition).ToList();

        //if (removedLastYPosition.Contains(previousYPosition * 2))
        //{
        //    removedLastYPosition = removedLastYPosition.Where(it => it < previousYPosition * 2).ToList();
        //}

        string result = String.Empty;
        foreach (var item in removedLastYPosition)
        {
            result += item + " , ";
        }

        float currentY = removedLastYPosition[Random.Range(0, removedLastYPosition.Count)];
        lastYPosition = currentY;

        return currentY;
    }

    private float GetRandomXPositionWithOutX(float previousXPosition)
    {
        List<float> xLists = xRange.ToList();
        int index = xLists.IndexOf(previousXPosition);

        if ((index - 1) >= 0)
            xLists.Remove(xRange[index - 1]);

        if ((index + 1) < xRange.Count())
            xLists.Remove(xRange[index + 1]);

        xLists.Remove(previousXPosition);

        float currentX = xLists[Random.Range(0, xLists.Count)];
        lastXPosition = currentX;

        return currentX;
    }

    private float GetRandomRotation()
    {
        float last = lastRotateAngle;
        float flipRotation = GetFlipRotation();
        List<float> rotationList = rotateAngles.ToList();

        if (last == 0)
        {
            rotationList = rotationList.Where(it => !it.Equals(last) && it.Equals(flipRotation)).ToList();
        }
        else
        {
            rotationList = rotationList.Where(it => !it.Equals(last)).ToList();
        }

        int index = Random.Range(0, rotationList.Count);
        float value = rotationList[index];
        lastRotateAngle = value;
        Debug.Log("GetRandomRotation : " + value);

        return value;
    }

    private float GetFlipRotation()
    {
        Vector2 ballPositiontoViewport = Camera.main.ViewportToWorldPoint(ball.transform.position);
        float currentAngle = ballPositiontoViewport.x < 0 ? rotateAngles[0] : rotateAngles[2];
        lastRotateAngle = currentAngle;
        return currentAngle;
    }

    private WallDirection GetWallDirection()
    {
        Vector2 ballPositiontoViewport = Camera.main.ViewportToWorldPoint(ball.transform.position);
        WallDirection direction = ballPositiontoViewport.x > 0 ? WallDirection.Left : WallDirection.Right;

        return direction;
    }

    #endregion
}
