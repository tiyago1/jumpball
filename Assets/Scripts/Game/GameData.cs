﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{
    public const int DEFAULT_HOOP_SCORE = 1;

    public int PerfectScoreCount;
    public int PottedBallCount;
    public int Score;
    public int HoopScore;
    public int StarCount;

    public int TotalPerfectScoreCount;

    public int DifficultyLevel
    {
        get
        {
            int level = 0;

            if (Score < 10)
                level = 1;

            if (PottedBallCount >= 3)
                level = 2;

            //if (Score < 20)
            //    level = 3;

            return level;
        }
    }

    public override string ToString()
    {
        string message = string.Format
            (
            "Score : {0} , DifficultyLevel : {1},PerfectScoreCount : {2} HoopScore : {3} PottedBallCount : {4}",
             Score, DifficultyLevel, PerfectScoreCount, HoopScore, PottedBallCount);

        return message;
    }

}
