﻿using UnityEngine;

public class CurrencyManager // Name is ridiculous
{
    public const int MAX_EACH_GAME_STAR_COUNT = 10;
    public const int TRIGGER_RATE = 2;
    private int starTriggerRate;

    public bool ShouldShowStar(GameData currentGameData, PlayerData playerData)
    {
        bool isShowStar = false;
        starTriggerRate++; // Yeri burası değil

        if (currentGameData.StarCount >= 10 || currentGameData.PottedBallCount < 2
            || starTriggerRate < TRIGGER_RATE)
        {
            return false;
        }
       
        starTriggerRate = 0;
        float randomValue = Random.value;
        isShowStar = randomValue < 0.5f ? false : true;

        return isShowStar;
    }
}