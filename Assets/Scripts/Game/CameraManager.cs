using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private const int MOTION_SPEED = 5;

    private Camera camera;

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        camera = this.GetComponent<Camera>();
    }

    public void ShakeCamera()
    {
        StartCoroutine(ShakeAnimation());
    }

    private IEnumerator ShakeAnimation()
    {
        camera.DOOrthoSize(camera.orthographicSize + 3, 0.3f);
        yield return new WaitForSeconds(0.3f);
        camera.DOOrthoSize(camera.orthographicSize - 3, 0.35f);
    }

    public void MoveCameraVerticalDirection(Vector3 position, Action callback)
    {
        StartCoroutine(MoveCamera(position, callback));
    }

    private IEnumerator MoveCamera(Vector3 position, Action callback)
    {
        Vector2 worldToViewPort = Camera.main.WorldToViewportPoint(position);

        while (worldToViewPort.y >= 0.35f)
        {
            float y = camera.transform.position.y + GameManager.VERTICAL_MOVEMENT_VALUE;

            Vector3 currentPosition = this.transform.position;
            currentPosition.y += 0.5f;
            this.transform.DOMove(currentPosition,0.2f);
            worldToViewPort = Camera.main.WorldToViewportPoint(position);
            yield return new WaitForEndOfFrame();
        }

        callback();
    }
}
