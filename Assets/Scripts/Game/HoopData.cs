﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoopData
{
    public Vector2 Position              { get; set; }
    public float Angle                   { get; set; }
    public bool IsVisible                { get; set; }
    public HoopType Type                 { get; set; }
    public bool HasStar                  { get; set; }
    public WallDirection? WallDirection  { get; set; }
}
