﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DifficultyType
{
    Easy,
    Normal,
    Hard,
    Harder,
    Extreme
}

public class DifficultyManager
{
    private List<RatioElement<HoopType>> hoopElements;
    private RandomRatioCalculater<HoopType> randomRatioCalculater;

    private int defaultHoopRatio;
    private int higherHoopRatio;
    private int angleHoopRatio;
    private int verticalHoopRatio;
    private int walledHoopRatio;

    public void Initialize()
    {
        hoopElements = new List<RatioElement<HoopType>>();
        UpdatetHoopElements();
    }

    private void UpdatetHoopElements()
    {
        if (hoopElements != null && hoopElements.Count > 0)
            hoopElements.Clear();

        RatioElement<HoopType> defaulthoop = new RatioElement<HoopType>(HoopType.Default, defaultHoopRatio);
        RatioElement<HoopType> higherhoop = new RatioElement<HoopType>(HoopType.Higher, higherHoopRatio);
        RatioElement<HoopType> angleHoop = new RatioElement<HoopType>(HoopType.Angle, angleHoopRatio);
        RatioElement<HoopType> verticalHoop = new RatioElement<HoopType>(HoopType.VerticalMotion, verticalHoopRatio);
        RatioElement<HoopType> walledHoop = new RatioElement<HoopType>(HoopType.Walled, walledHoopRatio);

        hoopElements.Add(defaulthoop);
        hoopElements.Add(higherhoop);
        hoopElements.Add(angleHoop);
        hoopElements.Add(verticalHoop);
        hoopElements.Add(walledHoop);
    }

    public HoopType GetNextHoopType()
    {
        randomRatioCalculater = new RandomRatioCalculater<HoopType>(hoopElements);
        RatioElement<HoopType> selectedHoop = randomRatioCalculater.GetRandomValue();
        
        return selectedHoop.Value;
    }

    public void UpdateDifficulty(GameData gameData)
    {
        DifficultyType difficulty = CalculateAndReturnDifficulty(gameData);
        CalculateHoopRatio(difficulty);
        UpdatetHoopElements();
    }

    private DifficultyType CalculateAndReturnDifficulty(GameData gameData)
    {
        DifficultyType difficulty = DifficultyType.Normal;

        if (gameData.Score > 60)
        {
            difficulty = DifficultyType.Extreme;
        }
        else if (gameData.Score > 40)
        {
            difficulty = DifficultyType.Harder;
        }
        else if (gameData.Score > 20)
        {
            difficulty = DifficultyType.Hard;
        }
        else if (gameData.Score > 6)
        {
            difficulty = DifficultyType.Normal;
        }
        else
        {
            difficulty = DifficultyType.Easy;
        }

        return difficulty;
    }

    private void CalculateHoopRatio(DifficultyType type)
    {
        ResetVariables();

        switch (type)
        {
            case DifficultyType.Easy:
                defaultHoopRatio = 100;
                break;
            case DifficultyType.Normal:
                defaultHoopRatio = 25;
                walledHoopRatio = 25;
                angleHoopRatio = 50;
                break;
            case DifficultyType.Hard:
                defaultHoopRatio = 10;
                angleHoopRatio = 25;
                walledHoopRatio = 25;
                higherHoopRatio = 40;
                break;
            case DifficultyType.Harder:
                defaultHoopRatio = 15;
                walledHoopRatio = 10;
                higherHoopRatio = 20;
                angleHoopRatio = 25;
                verticalHoopRatio = 30;
                break;
            case DifficultyType.Extreme:
                angleHoopRatio = 30;
                higherHoopRatio = 20;
                verticalHoopRatio = 50;
                break;
            default:
                Debug.LogError("Impossible difficultyType!");
                break;
        }
    }

    private void ResetVariables()
    {
        defaultHoopRatio = 0;
        angleHoopRatio = 0;
        higherHoopRatio = 0;
        verticalHoopRatio = 0;
        walledHoopRatio = 0;
    }
}