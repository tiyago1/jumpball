using DG.Tweening;
using DG.Tweening.Plugins.Core.PathCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Hoop : MonoBehaviour
{
    #region Variables

    public bool IsPerfectScore { get; set; } = true;

    [SerializeField] private GameObject triggerColliderObject;
    [SerializeField] private GameObject dotContainer;
    [SerializeField] private Animator hoopedAnimator;
    [SerializeField] private Star star;
    [SerializeField] private GameObject[] walls;
    [SerializeField] private ParticleSystem scoreParticle;

    private SpriteRenderer spriteRenderer;
    private Coroutine verticalMovementMotion;

    #endregion

    #region Unity Methods

    private void Awake()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    #endregion

    #region Public Methods

    public void UpdateData(HoopData data)
    {
        SetPosition(data.Position);
        SetRotation(data.Angle);
        ResetHoop();
        SetVisibilty(data.IsVisible);
        SetStarVisibilty(data.HasStar);

        if (data.Type == HoopType.VerticalMotion)
        {
            PlayVerticalMotion();
        }

        if (data.Type == HoopType.Walled)
        {
            ShowWalledPosition(data.WallDirection.Value);
        }
    }

    public void SetVisibilty(bool isVisible)
    {
        this.gameObject.SetActive(isVisible);
    }

    private void ResetHoop()
    {
        IsPerfectScore = true;
        SetColor(Color.white, 0);
        SetTriggerActive(true);
        SetDotsActive(false);
        HideWalls();
    }

    public void SetTriggerActive(bool isActive)
    {
        triggerColliderObject.SetActive(isActive);
    }

    public void SetColor(Color color, float time)
    {
        spriteRenderer.DOKill();
        spriteRenderer.DOColor(color, time);
    }

    private void PlayVerticalMotion()
    {
        Vector3 newPosition = this.transform.position;
        newPosition.y -= 0.2f;
        dotContainer.transform.position = newPosition;
        SetDotsActive(true);

        verticalMovementMotion = StartCoroutine(MoveVerticalMotion(this.transform.position));
    }

    public void StopVerticalMotion()
    {
        if (verticalMovementMotion != null)
        {
            StopCoroutine(verticalMovementMotion);
            this.transform.DOKill();
        }
    }

    public void PlayHoopedAnimation()
    {
        hoopedAnimator.ResetTrigger("Play");
        hoopedAnimator.SetTrigger("Play");
    }

    public void SetStarVisibilty(bool isVisible)
    {
        star.SetVisibilty(isVisible);
    }

    public Star GetStar()
    {
        return star;
    }

    public void PlayScroreParticle()
    {
        return;
        scoreParticle.Play();
    }

    #endregion

    #region Private Methods

    private void SetPosition(Vector2 newPosition)
    {
        this.transform.localPosition = newPosition;
    }

    private void SetRotation(float angle)
    {
        this.transform.eulerAngles = new Vector3(0, 0, angle);
    }

    private void SetDotsActive(bool isActive)
    {
        dotContainer.SetActive(isActive);
    }

    private void ShowWalledPosition(WallDirection wallDirection)
    {
        walls[(int)wallDirection].SetActive(true);
    }

    private void HideWalls()
    {
        Array.ForEach(walls, it => it.gameObject.SetActive(false));
    }

    private IEnumerator MoveVerticalMotion(Vector3 position)
    {
        yield return null;
        Vector3 first = position;
        first.y += 1.5f;

        Vector3 second = first;
        second.y -= 1.5f;

        Vector3 third = position;
        third.y -= 1.5f;

        Vector3[] waypoints = new Vector3[3]
        {
            first,
            second,
            third,
        };

        while (true)
        {
            Path path = new Path(PathType.Linear, waypoints, 0);
            var action = transform.DOPath(path, 2.0f, PathMode.TopDown2D);
            yield return new WaitForSeconds(action.Duration());
            Array.Reverse(waypoints);
        }
    }

    #endregion

    #region Events

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Ball"))
        {
            JumpLogger.Log("It is not perfect score!");
            IsPerfectScore = false;
        }
    }

    #endregion
}
