﻿public class PlayerData
{
    public GameData[] Last3Game { get; set; }

    public int StarCount { get; set; }
    public int TotalGameCount { get; set; }
    public int HighScore { get; set; }
    public int HighStreakCount { get; set; }
}