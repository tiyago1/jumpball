﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Star : MonoBehaviour
{
    public Transform UIStarPoint;
    private Animator animator;

    public event Action OnStarReachedToTarget;

    public void Awake()
    {
        animator = this.GetComponent<Animator>();
    }

    public void SetVisibilty(bool isVisible)
    {
        this.gameObject.SetActive(isVisible);
    }

    public void HideStar()
    {
        if (this.gameObject.activeInHierarchy)
        {
            StartCoroutine(WaitAndHideStar(0.5f));
        }
    }

    private IEnumerator WaitAndHideStar(float seconds)
    {
        animator.SetTrigger("Hide");
        yield return new WaitForSeconds(seconds);
        SetVisibilty(false);
    }

    public void GoToTarget(Transform target)
    {
        animator.enabled = false;
        this.transform.DOKill();
        var c = this.transform.DOMove(target.position, 0.5f);
        c.OnComplete(() => StarArrived());
    }

    private void StarArrived()
    {
        animator.enabled = true;
        SetVisibilty(false);
        if (OnStarReachedToTarget != null)
        {
            OnStarReachedToTarget();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ball"))
        {
            GoToTarget(UIStarPoint);
        }
    }
}
