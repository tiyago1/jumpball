﻿using System;
using System.Reflection;
using UnityEngine;

public static class JumpLogger
{
    public static void Log<T>(T message, bool isClearConsoleBefore = false)
    {
        if (isClearConsoleBefore)
            //ClearConsole();

        Debug.Log(message.ToString());
        
    }

    public static void ClearConsole()
    
    {
#if UNITY_EDITOR
        Type logEntries = Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");
        MethodInfo clearMethod = logEntries.GetMethod("Clear", BindingFlags.Static | BindingFlags.Public);
        clearMethod.Invoke(null, null);
#endif
    }
}
