﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallModel : BallElement
{
    #region Fields

    public int MaxPower;
    public int MinPower;
    public float Power;
    public float RealeseDistanceRange;

    public Vector2 Position
    {
        get
        {
            return this.transform.position;
        }
    }
    public Vector2 Direction;
    public Transform OriginPoint;

    public Vector2 Velocity
    {
        get
        {
            return Direction * Power;
        }
    }

    public List<Transform> TrajectoryDots;

    public Vector2 InputPosition;
    public Vector2 PottedHoopPosition;

    public SpriteRenderer SpriteRenderer;
    public LineRenderer LineRenderer;
    public TrailRenderer TrailRenderer;
    public ParticleSystem CollisionParticleSystem;

    #endregion

    public BallModel(int maxPower, int minPower, float realseDistanceRange)
    {
        MaxPower = maxPower;
        MinPower = minPower;
        RealeseDistanceRange = realseDistanceRange;
    }
}