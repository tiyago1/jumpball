﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BallEvents))]
[RequireComponent(typeof(BallModel))]
public class BallDrag : BallElement
{
    private BallEvents events { get; set; }
    private BallModel model { get; set; }

    protected override void InitializeComponent()
    {
        events = BallSystem.Components.Events;
        model = BallSystem.Components.Model;
    }

    protected override void InitializeEvents()
    {
        events.OnDraging += Events_OnDragExecuted;
    }

    private void Events_OnDragExecuted()
    {
        Drag();
    }

    private void Drag()
    {
        this.transform.position = GetClampedPosition(model.InputPosition);
    }

    private Vector2 GetClampedPosition(Vector2 inputPosition)
    {
        Vector2 originPoint = model.OriginPoint.position;
        Vector2 offset = inputPosition - originPoint;
        float clampedRadius = Mathf.Clamp(offset.magnitude, 0, model.RealeseDistanceRange);

        Vector2 newPosition = originPoint + (offset.normalized * clampedRadius);
        return newPosition;
    }
}
