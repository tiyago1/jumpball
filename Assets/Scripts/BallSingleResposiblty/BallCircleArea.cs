﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallEvents))]
public class BallCircleArea : BallElement
{
    #region Fields

    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Transform circleTransform;

    [SerializeField] private float scale;
    [SerializeField] private float colorA;
    [SerializeField] private Color color;

    #endregion

    #region Components
    
    private BallEvents events { get; set; }
    private BallModel model { get; set; }

    #endregion

    protected override void InitializeComponent()
    {
        events = BallSystem.Components.Events;
        model = BallSystem.Components.Model;
    }

    protected override void InitializeEvents()
    {
        events.OnDraging += Events_OnDraging;
        events.OnPointerUpExecuted += Events_OnPointerUpExecuted;
    }

    private void Events_OnPointerUpExecuted()
    {
        color = new Color(1, 1, 1, 0);
        circleTransform.localScale = Vector3.zero;
    }

    private void Events_OnDraging()
    {
        Vector2 originPoint = model.OriginPoint.position;
        Vector2 offset = model.InputPosition - originPoint;
        
        float value = Mathf.Clamp(offset.magnitude + scale, 2, 6); 
        float colorValue = Mathf.Clamp(offset.magnitude * colorA, 0, 0.5f);

        color = new Color(1, 1, 1, colorValue);
        spriteRenderer.color = color;
        circleTransform.localScale = new Vector3(value, value, value);
    }
}
