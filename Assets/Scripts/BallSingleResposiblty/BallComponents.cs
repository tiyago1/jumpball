﻿using System.Collections.Generic;
using UnityEngine;

public class BallComponents 
{
    public readonly BallCircleArea CircleArea;
    public readonly BallDrag Drag;
    public readonly BallEvents Events;
    public readonly BallLauncher Launcher;
    public readonly BallModel Model;
    public readonly BallPower Power;
    public readonly BallTrajectory Trajectory;
    public readonly BallView View;

    public IEnumerable<BallElement> GetElements()
    {
        yield return CircleArea;
        yield return Drag;
        yield return Events;
        yield return Launcher;
        yield return Model;
        yield return Power;
        yield return Trajectory;
        yield return View;
    }                      

    public BallComponents(GameObject ball)
    {
        CircleArea  = ball.GetComponent<BallCircleArea>();
        Drag        = ball.GetComponent<BallDrag>();
        Events      = ball.GetComponent<BallEvents>();
        Launcher    = ball.GetComponent<BallLauncher>();
        Model       = ball.GetComponent<BallModel>();
        Power       = ball.GetComponent<BallPower>();
        Trajectory  = ball.GetComponent<BallTrajectory>();
        View        = ball.GetComponent<BallView>();
    }
}
