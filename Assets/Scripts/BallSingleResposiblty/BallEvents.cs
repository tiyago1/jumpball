﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BallModel))]
public class BallEvents : BallElement, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    #region Event Actions

    public event Action OnPointerUpExecuted;
    public event Action OnPointerDownExecuted;
    public event Action OnDraging;
    public event Action OnBallPotted;
    public event Action OnStarGained;
    public event Action OnBallFalledDown;
    public event Action<Collision2D> OnBallCollisionDetected;

    #endregion

    #region Components

    private BallModel model { get; set; }

    #endregion

    #region Element Methods

    protected override void InitializeComponent()
    {
        model = BallSystem.Components.Model;
    }

    #endregion

    #region Events

    public void OnPointerDown(PointerEventData eventData)
    {
        if (OnPointerDownExecuted != null)
        {
            OnPointerDownExecuted();
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        model.InputPosition = Camera.main.ScreenToWorldPoint(eventData.position);
        if (OnDraging != null)
        {
            OnDraging();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (OnPointerUpExecuted != null)
        {
            OnPointerUpExecuted();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("HoopTrigger"))
        {
            model.PottedHoopPosition = collision.gameObject.transform.position;
            if (OnBallPotted != null)
            {
                OnBallPotted();
            }
        }

        if (collision.CompareTag("Star"))
        {
            if (OnStarGained != null)
            {
                OnStarGained();
            }
        }

        if (collision.CompareTag("GameOverTrigger"))
        {
            if (OnBallFalledDown != null)
            {
                OnBallFalledDown();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (OnBallCollisionDetected != null)
        {
            OnBallCollisionDetected(collision);
        }
    }

    #endregion
}
