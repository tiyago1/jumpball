﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallPower))]
[RequireComponent(typeof(BallDrag))]
[RequireComponent(typeof(BallEvents))]
[RequireComponent(typeof(BallModel))]
public class BallTrajectory : BallElement
{
    private BallEvents events { get; set; }
    private BallModel model { get; set; }

    protected override void InitializeComponent()
    {
        events = BallSystem.Components.Events;
        model = BallSystem.Components.Model;
    }

    protected override void InitializeEvents()
    {
        events.OnDraging += Events_OnDragExecuted;
        events.OnPointerUpExecuted += Events_OnPointerUpExecuted;
    }

    private void Events_OnPointerUpExecuted()
    {
        ResetTrajectoryPoints();
    }

    private void Events_OnDragExecuted()
    {
        SetTrajectoryPoints(this.transform.position, model.Velocity);
    }

    private void ResetTrajectoryPoints()
    {
        model.TrajectoryDots.ForEach(it => it.gameObject.SetActive(false));
    }

    private void SetTrajectoryPoints(Vector3 startPosition, Vector2 initialVelocity)
    {
        model.TrajectoryDots.ForEach(it => it.gameObject.SetActive(true));

        float velocity = Mathf.Sqrt((initialVelocity.x * initialVelocity.x) + (initialVelocity.y * initialVelocity.y));
        float angle = Mathf.Rad2Deg * (Mathf.Atan2(initialVelocity.y, initialVelocity.x));
        float time = 0;
        Vector3 defaultScale = new Vector3(0.9f, 0.9f, 0.9f);
        time += 0.1f;
        for (int i = 0; i < model.TrajectoryDots.Count; i++)
        {
            float perTimeXPosition = velocity * time * Mathf.Cos(angle * Mathf.Deg2Rad);
            float perTimeYPosition = velocity * time * Mathf.Sin(angle * Mathf.Deg2Rad) - (Physics2D.gravity.magnitude * time * time / 2.0f);
            Vector2 position = new Vector2(startPosition.x + perTimeXPosition, startPosition.y + perTimeYPosition);
            float scaleFactor = i * 0.18f;
            model.TrajectoryDots[i].position = position;
            model.TrajectoryDots[i].localScale = defaultScale - new Vector3(scaleFactor, scaleFactor, scaleFactor);
            model.TrajectoryDots[i].gameObject.SetActive(true);
            model.TrajectoryDots[i].eulerAngles = new Vector3(0, 0, Mathf.Atan2(initialVelocity.y - (Physics.gravity.magnitude) * time, initialVelocity.x) * Mathf.Rad2Deg);
            time += 0.1f;
        }
    }
}
