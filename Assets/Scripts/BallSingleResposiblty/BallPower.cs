﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BallEvents))]
[RequireComponent(typeof(BallModel))]
public class BallPower : BallElement
{
    #region Components

    private BallEvents events { get; set; }
    private BallModel model { get; set; }

    #endregion

    protected override void InitializeComponent()
    {
        events = BallSystem.Components.Events;
        model = BallSystem.Components.Model;
    }

    protected override void InitializeEvents()
    {
        events.OnDraging += Events_OnDragExecuted;
    }

    private void Events_OnDragExecuted()
    {
        CalculatePower(GetOriginToInputOffsetMagnitude(model.InputPosition));
    }

    private float GetOriginToInputOffsetMagnitude(Vector2 inputPosition)
    {
        Vector2 originPoint = model.OriginPoint.position;
        Vector2 offset = inputPosition - originPoint;

        return offset.magnitude;
    }

    private void CalculatePower(float powerValue)
    {
        float percentageDistance = (powerValue / model.RealeseDistanceRange) * 100;

        if (percentageDistance <= 101)
        {
            model.Power = model.MaxPower * percentageDistance / 100;
            if (model.Power < model.MinPower)
            {
                model.Power = model.MinPower;
            }
        }
    }
}
