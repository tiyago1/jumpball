﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BallEvents))]
public class BallView : BallElement
{
    private BallEvents events { get; set; }
    private BallModel model { get; set; }

    protected override void InitializeComponent()
    {
        events = BallSystem.Components.Events;
        model = BallSystem.Components.Model;
    }

    protected override void InitializeEvents()
    {
        events.OnPointerUpExecuted += Events_OnPointerUpExecuted;
        events.OnPointerDownExecuted += Events_OnPointerDownExecuted;
        events.OnBallPotted += Events_OnBallPotted;
        events.OnBallCollisionDetected += Events_OnBallCollisionDetected;
    }

    private void Events_OnBallCollisionDetected(Collision2D collision)
    {
        if (collision.collider.CompareTag("ScreenCollider"))
        {
            model.CollisionParticleSystem.transform.position = collision.contacts[0].point;
            model.CollisionParticleSystem.Play();
        }
    }

    private void Events_OnBallPotted()
    {
        model.TrailRenderer.enabled = false;
    }

    private void Events_OnPointerDownExecuted()
    {

    }

    private void Events_OnPointerUpExecuted()
    {
        model.TrailRenderer.enabled = true;
    }
}
