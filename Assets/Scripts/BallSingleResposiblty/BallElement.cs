﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BallElement : MonoBehaviour
{
    protected Rigidbody2D rigidbody2D;

    private void Start()
    {
        Initialize();
    }

    protected virtual void Initialize()
    {
        rigidbody2D = this.GetComponent<Rigidbody2D>();
        InitializeComponent();
        InitializeEvents();
    }

    protected virtual void InitializeComponent()
    {

    }

    protected virtual void InitializeEvents()
    {

    }
}
