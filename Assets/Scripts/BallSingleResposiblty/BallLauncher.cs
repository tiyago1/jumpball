﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BallEvents))]
[RequireComponent(typeof(BallModel))]
[RequireComponent(typeof(BallPower))]
public class BallLauncher : BallElement
{
    [SerializeField]
    private CircleCollider2D circleCollider2D;

    #region Components

    private BallEvents events { get; set; }
    private BallModel model { get; set; }

    #endregion

    protected override void InitializeComponent()
    {
        events = BallSystem.Components.Events;
        model = BallSystem.Components.Model;
    }

    protected override void InitializeEvents()
    {
        events.OnPointerDownExecuted += Events_OnPointerDownExecuted;
        events.OnPointerUpExecuted += Events_OnPointerUpExecuted;
        events.OnDraging += Events_OnDragExecuted;
    }

    private void Events_OnDragExecuted()
    {
        CalculateVelocity(this.transform.position);
    }

    private void Events_OnPointerUpExecuted()
    {
        rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
        ReleaseBall();
    }

    private void Events_OnPointerDownExecuted()
    {
        rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
        rigidbody2D.velocity = Vector2.zero;
    }

    private void ReleaseBall()
    {
        rigidbody2D.velocity = model.Velocity;
        circleCollider2D.radius = 0.5f;
    }

    private void CalculateVelocity(Vector2 currentPosition)
    {
        Vector2 centerPosition = model.OriginPoint.position;
        model.Direction = (currentPosition - centerPosition).normalized * Vector2.one * -1;
    }

    public void ResetLauncher()
    {
        this.transform.parent.position = model.PottedHoopPosition;
        this.transform.position = model.OriginPoint.position;
        rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
        rigidbody2D.velocity = Vector2.zero;
        model.PottedHoopPosition = Vector2.zero;
        circleCollider2D.radius = 1.0f;
        model.Power = 0;
    }
}
