﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSystem : MonoBehaviour
{
    public static BallComponents Components;

    public void Awake()
    {
        Components = new BallComponents(this.gameObject);
    }
}
