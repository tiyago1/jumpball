﻿using System.Collections;
using TMPro;
using DG.Tweening;
using UnityEngine;
using System;

public class HoopTextCustomizeData
{
    public Color Color = Color.white;
    public float HoopDistance = 3;
}

public class UIManager : MonoBehaviour
{
    #region Fields

    [SerializeField] private TextMeshProUGUI globalScoreText;
    [SerializeField] private TextMeshProUGUI starText;
    [SerializeField] private TextMeshPro perfectText;
    [SerializeField] private TextMeshPro scoreText;
    [SerializeField] private TextMeshPro hoopText;
    [SerializeField] private GameObject logoImage;
    [SerializeField] private CanvasGroup[] hideableObjectsForStarter;

    #endregion

    #region Unity Methods

    public void Start()
    {
        Initialize();
    }

    #endregion

    #region Public Methods

    public void Initialize()
    {
        globalScoreText.gameObject.SetActive(false);
        ShowLogoAnimation();
    }

    public void HideObjectsForStarter()
    {
        foreach (var item in hideableObjectsForStarter)
        {
            item.DOKill();
            item.DOFade(0, 0.3f);
        }
    }

    public void ShowObjectsForStarter()
    {
        globalScoreText.gameObject.SetActive(true);
    }

    public void UpdateStarText(int starCount)
    {
        starText.text = starCount.ToString();
    }

    private void UpdateGlobalScoreText(int score)
    {
        Color defaultColor = new Color(1, 1, 1, 0.34f);
        globalScoreText.color = new Color(1, 1, 1, 0.34f);
        var animationEvent = globalScoreText.DOColor(Color.white, 0.5f);
        animationEvent.onComplete = () => globalScoreText.DOColor(defaultColor, 0.3f);
        globalScoreText.text = score.ToString();
    }

    public void ShowHoopScores(GameData data, Vector2 position)
    {
        JumpLogger.Log(data);
        StopAllCoroutines();
        StartCoroutine(HoopScoreShowingCylcle(data, position));
    }

    #endregion

    #region Private Methods

    private void ShowLogoAnimation()
    {
        logoImage.transform.DOBlendableScaleBy(Vector3.one, 0.2f);
    }

    private IEnumerator HoopScoreShowingCylcle(GameData data, Vector2 position)
    {
        if (data.PerfectScoreCount > 0)
        {
            ShowHoopPerfectText(data.PerfectScoreCount, position);
            yield return new WaitForSeconds(0.2f);
        }

        UpdateGlobalScoreText(data.Score);
        ShowHoopScoreText(data.HoopScore, position);
    }

    private void ShowHoopPerfectText(int perfectScoreCount, Vector2 position)
    {
        HoopTextCustomizeData customizeData = new HoopTextCustomizeData()
        {
            Color = Color.red
        };

        string message = "Perfect x" + perfectScoreCount;
        ShowHoopText(position, message, perfectText, customizeData);
    }

    private void ShowHoopScoreText(int score, Vector2 position)
    {
        HoopTextCustomizeData customizeData = new HoopTextCustomizeData()
        {
            HoopDistance = 2
        };
        string message = String.Format("+{0}", score);
        ShowHoopText(position, message, scoreText, customizeData);
    }

    private void ShowHoopText(Vector2 position, string message, TextMeshPro text, HoopTextCustomizeData customizeData = null)
    {
        ResetHoopText(text);

        if (customizeData != null)
            CustomizeHoopText(customizeData);
        else
            customizeData = new HoopTextCustomizeData();

        text.transform.position = position;
        text.text = message;
        text.transform.DOLocalJump(new Vector3(position.x, position.y + customizeData.HoopDistance, 0), 1, 1, 0.8f);
        var doFadeEvent = text.DOFade(1, 0.5f);
        doFadeEvent.onComplete = () => text.transform.DOScale(0, 0.3f);
    }

    private void CustomizeHoopText(HoopTextCustomizeData data)
    {
        hoopText.color = data.Color;
    }

    private void ResetHoopText(TextMeshPro text)
    {
        text.transform.localScale = Vector3.one;
    }

    #endregion

    #region Events

    public void OnRestartButtonClicked()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    #endregion
}